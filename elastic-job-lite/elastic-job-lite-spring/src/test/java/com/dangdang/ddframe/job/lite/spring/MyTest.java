package com.dangdang.ddframe.job.lite.spring;

import com.dangdang.ddframe.job.api.ElasticJob;
import com.google.common.base.Optional;

/**
 * @Author: wangxiaow@nicomama.com
 * @Date: 2019-10-21
 */
public class MyTest {

    public static void main(String[] args) {
        Optional<ElasticJob> absent = Optional.absent();
        System.out.println(absent);
    }
}
